package uas.pemrograman2.config;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author ahaidaralbaqir
 */
public final class Database {
    
   public static final String NAME = "uas_ahmadhaidaralbaqir";
   public static final String PASSWORD = "password";
   public static final String HOST = "localhost";
   public static final String USER = "root";
   public static final String PORT = "3306";
}
