/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package uas.pemrograman2.dao;

import java.sql.*;
import java.sql.SQLException;
import uas.pemrograman2.database.Connection;
/**
 *
 * @author ahaidaralbaqir
 */
public class Employee {
    public void paySalary(String nik, String name, int duration, int salary) throws Exception{
        try {
                String sql = "INSERT INTO gaji VALUES('"+nik+"','"+name+"','"+duration+"','"+salary+"')";
                PreparedStatement stmt = Connection.GetConnection().prepareStatement(sql);
                stmt.execute(); 
        } catch (ClassNotFoundException | SQLException e) {
                throw e;
        }
    }
    
    public void removeSalary(String nik) throws Exception {
        try {
            Statement stmt = Connection.GetConnection().createStatement();
            String sql = "DELETE FROM gaji WHERE nik = '" +nik+ "'";
            stmt.executeUpdate(sql);
        } catch (ClassNotFoundException | SQLException e) {
                throw e;
        }
    }
    
    public void updateSalary(String nik, String name, int workDuration, int salary) throws Exception {
        try {
                String sql = "UPDATE gaji SET nm_karyawan = ?, lama_kerja = ?, gaji = ? WHERE nik = ?";
                PreparedStatement stmt = Connection.GetConnection().prepareStatement(sql);
                stmt.setString(1,  name);
                stmt.setInt(2, workDuration);
                stmt.setInt(3, salary);
                stmt.setString(4, nik);
                stmt.execute(); 
        } catch (ClassNotFoundException | SQLException e) {
                throw e;
        }
    }    
    public ResultSet viewSalaries() throws Exception {
        try {
                String sql = "select * from gaji";
                PreparedStatement stmt = Connection.GetConnection().prepareStatement(sql);
                ResultSet result = stmt.executeQuery();
                return result;
        } catch (ClassNotFoundException | SQLException e) {
                throw e;
        } 
    }
}
