/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package uas.pemrograman2.database;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import uas.pemrograman2.config.Database;
/**
 *
 * @author ahaidaralbaqir
 */
public class Connection {
    public static java.sql.Connection con;
    public static Statement statement;
    public static java.sql.Connection GetConnection() throws ClassNotFoundException, SQLException{
        if (con != null) {
            return con;
        }
        // Load config file
        String url = String.format("jdbc:mysql://%s:%s/%s", Database.HOST, Database.PORT, Database.NAME);
        String username = Database.USER;
        String password = Database.PASSWORD;
        java.sql.Connection connect = DriverManager.getConnection(url, username, password);
        System.out.println("Connection Established successfully");  
        con = connect;
        return con;
    }
}
